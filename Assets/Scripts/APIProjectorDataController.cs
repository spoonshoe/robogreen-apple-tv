﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Strackaline;
using UnityEngine.Events;
using System;
using Doozy.Engine.UI;
using Doozy.Integrations;

public class APIProjectorDataController : MonoBehaviour
{

    public static event Action ProjectoDataLoaded;
    public static event Action ProjectoDataNeedsReload;
    public static event Action ProjectionDataUpdatedComplete;

    public UnityWebRequest apiRequest;
    public ProjectorData projectorData;
    public ProjectorController projectorController;
    public CourseManager courseManager;
    public bool checkForNewData;
    public GameObject masterCanvas;
    public IEnumerator PingAPIForNewPuttDataCO;
    public PuttManager puttManager;

    public void Awake()
    {
        puttManager = FindObjectOfType<PuttManager>();
        projectorController = FindObjectOfType<ProjectorController>();
        courseManager = FindObjectOfType<CourseManager>();
        PingAPIForNewPuttDataCO = PingAPIForNewPuttData();
        UserController.UserLoggedInComplete += StartPingAPIForNewPuttData;
    }

    public void StartPingAPIForNewPuttData()
    {
        StartCoroutine(PingAPIForNewPuttDataCO);
        UIView.HideView("RoboGreen", "Background");
    }

    IEnumerator PingAPIForNewPuttData()
    {
        while (checkForNewData)
        {
            yield return new WaitForSeconds(1);
            ProjectorDataSubmit();
        }
    }


    public void ProjectorDataSubmit()
    {
        string finalURL = APIGlobal.Inst.baseURL + "GetPuttTrainerConfig(puttTrainerId=" + APIGlobal.Inst.puttTrainerId.ToString() + ",configTypeId=1)";
        apiRequest = UnityWebRequest.Get(finalURL);
        apiRequest.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        apiRequest.SetRequestHeader("Authorization", "Token " + APIGlobal.Inst.apiToken);
        StartCoroutine(ProjectorDataRequest());
    }

    IEnumerator ProjectorDataRequest()
    {
        using (apiRequest)
        {

            yield return apiRequest.SendWebRequest();
            if (apiRequest.isNetworkError || apiRequest.isHttpError)
            {
                FindObjectOfType<AlertPopupController>().ShowAlertPopup("Error", "Unable to retrieve projector data.");
            }
            else
            {
                projectorData = JsonUtility.FromJson<ProjectorData>(apiRequest.downloadHandler.text);
                projectorController.projectorStatusData = JsonUtility.FromJson<ProjectorStatusData>(projectorData.value[0].configurationData);
                projectorController.greenActivity = projectorController.projectorStatusData.greenActivity;
                projectorController.settings = projectorController.projectorStatusData.settings;

                courseManager.selectedGreenID = projectorController.projectorStatusData.greenActivity.greenId;
                

                if (projectorData.value[0].configurationId != projectorController.configurationId)
                {
                    ProjectoDataNeedsReload?.Invoke();
                    projectorController.configurationId = projectorData.value[0].configurationId;

                }
                else
                {
                    ProjectoDataLoaded?.Invoke();
                }

                ProjectionDataUpdatedComplete?.Invoke();
            }
        }
    }
}
