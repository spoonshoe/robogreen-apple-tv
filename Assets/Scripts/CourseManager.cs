﻿using UnityEngine;
using Strackaline;
using UnityEngine.Events;
using System;
using Strackaline;

public class CourseManager : MonoBehaviour
{
    public Region region;
	public Course course;
    public CourseGreens courseGreens;
    public Green green;


    public Region currRegion;
	public Course currCourse;
    public Green currGreen;
    public GameObject greenModelLoaded;

    public GreenArrow arrowsData;
    public Contour contourData;

    public string selectedRegionCode;
    public string selectedCourseID;
    public string selectedGroupID;
    public string selectedGreenID;
}
