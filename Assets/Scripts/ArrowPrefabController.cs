﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;

public class ArrowPrefabController : MonoBehaviour 
{

    public float speed;
    public bool isDoneFading;
    public bool isDoneAnimating;
    public float slope;
    public Color slopeColor;
    public string hex;
    public Arrow arrow;

}
