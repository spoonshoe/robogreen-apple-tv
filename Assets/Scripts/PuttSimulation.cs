﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;


public class PuttSimulation : MonoBehaviour
{
    public Putt putt;

    public List<Vector3> puttPointsFull = new List<Vector3>();
    public List<Vector3> puttPointsHalved = new List<Vector3>();

    public void SimInit()
    {
        Debug.Log("Sim init");
        GetPuttData();
        GetSimplePuttPath();
        GetHalfPuttPoints();

        GetComponent<LineToHole>().DrawLineToHole();
        GetComponent<LineToTarget>().SetLineToTargetMarkers();
    }

    public void GetPuttData()
    {

        Debug.Log("get putt data called");
        for (int c = 0; c < putt.points.Length; c++)
        {
            Point point = putt.points[c];
            puttPointsFull.Add(new Vector3(point.location.x, point.location.z, point.location.y));
        }
    }

    public List<Vector3> GetHalfPuttPoints()
    {
        Debug.Log("GetHalfPuttPointsa called");
        for (int i = 0; i < puttPointsFull.Count; i += 2)
        {
            puttPointsHalved.Add(puttPointsFull[i]);
        }
        return puttPointsHalved;
    }

    public List<Vector3> GetSimplePuttPath()
    {
        Debug.Log("GetSimplePuttPath called");
        List<Vector3> _simplePoints = new List<Vector3>();
        for (int i = 0; i < puttPointsFull.Count; i += 30)
        {
            _simplePoints.Add(puttPointsFull[i]);
        }
        return _simplePoints;
    }

}
