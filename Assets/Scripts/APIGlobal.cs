﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APIGlobal : MonoBehaviour
{

    private static APIGlobal _inst;

    public static APIGlobal Inst
    {
        get
        {
            if (_inst == null)
            {
                _inst = FindObjectOfType<APIGlobal>();
            }

            return _inst;
        }
    }



    public string baseURL;
    public string token;
    public string apiToken;
    public int puttTrainerId;

   
}
