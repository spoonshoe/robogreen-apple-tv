﻿using System.Collections;
using System.Collections.Generic;
using Strackaline;
using UnityEngine;


public class PuttInteractive : MonoBehaviour
{
    private PuttVariation _puttVariation;

    [SerializeField]
    public PuttVariation PuttVariation
    {
        get => _puttVariation;
        set
        {
            if (_puttVariation == value) return;
            _puttVariation = value;

            CreatePuttSimulations();
        }
    }

    public GameObject ballDragInput;
    public GameObject holeDragInput;

    public Vector3 ballLocation;
    public Vector3 holeLocation;
    public bool isActive;

    public List<PuttSimulation> puttSimulationsList = new List<PuttSimulation>();
    public GameObject puttSimulationPrefab;
    public Material invalidPuttMaterial;
    public Material validPuttMaterial;



    public void LoadNewPutt()
    {
        GetComponent<APIPuttController>().LoadPuttWithFirmness(ballLocation, holeLocation);
    }


    void CreatePuttSimulations()
    {
       foreach (Putt item in PuttVariation.value)
        {
            if(!item.isValidPutt)
            {
                SetInvalidPuttMaterial();
                return;
            }


            GameObject inst = Instantiate(puttSimulationPrefab);
            inst.GetComponent<LineToTarget>().ClearLineToTarget();
            inst.transform.SetParent(gameObject.transform);
            inst.transform.localPosition = Vector3.zero;
            inst.GetComponent<PuttSimulation>().putt = item;
            puttSimulationsList.Add(inst.GetComponent<PuttSimulation>());
            inst.GetComponent<PuttSimulation>().SimInit();
        }
        SetValidePuttMaterial();
        GetComponent<PuttAssistance>().CreateStanceBox();
    }

    public void ClearSimulationLines()
    {
        foreach (PuttSimulation item in puttSimulationsList)
        {
            item.GetComponent<LineToTarget>().ClearLineToTarget();
            Destroy(item.gameObject);
        }
        GetComponent<PuttAssistance>().ClearPuttStance();
        puttSimulationsList.Clear();
    }

    void SetInvalidPuttMaterial()
    {
        transform.GetChild(0).GetComponent<MeshRenderer>().material = invalidPuttMaterial;
    }

    void SetValidePuttMaterial()
    {
        transform.GetChild(0).GetComponent<MeshRenderer>().material = validPuttMaterial;
    }
}
