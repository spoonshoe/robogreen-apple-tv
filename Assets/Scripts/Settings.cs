﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Strackaline
{
    [System.Serializable]
    public class Settings
    {
        public List<GreenSetting> greenSettings = new List<GreenSetting>();
    }

    [System.Serializable]
    public class GreenSetting
    {
        public string settingName;
        public string settingValue;
        public bool slider;
        public bool toggle;

        public int minValue;
        public int maxValue;

    }


}