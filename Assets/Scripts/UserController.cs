﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Strackaline;
using Doozy.Engine;
using Doozy.Engine.UI;
using Doozy.Engine.UI.Input;
using Doozy.Integrations;
using System;
public class UserController : MonoBehaviour
{

    public static event Action UserLoggedInComplete;

    public User user;
    public UIView login;


    private void Awake()
    {
        LoadUserFromLocalFile();
    }

    public void SaveUserToLocalFile(string _user)
    {
        user = JsonUtility.FromJson<User>(_user);

        PlayerPrefs.SetString("LoggedIn", "True");
        PlayerPrefs.SetString("Username", user.memberDetail.emailAddress);
        PlayerPrefs.SetString("Password", "");
        PlayerPrefs.SetString("Token", user.token);
        PlayerPrefs.SetString("DateTimeLoggedIn", DateTime.Now.ToString());
        PlayerPrefs.SetString("MemberId", user.memberId.ToString());
        PlayerPrefs.SetString("PuttTrainerId", user.puttTrainers[0].puttTrainerId.ToString());
        PlayerPrefs.SetString("PuttTrainerTypeId", user.puttTrainers[0].puttTrainerTypeId.ToString());
        PlayerPrefs.SetString("PuttTrainerTypeName", user.puttTrainers[0].puttTrainerTypeName.ToString());
        PlayerPrefs.SetString("PuttTrainerName", user.puttTrainers[0].name.ToString());

        APIGlobal.Inst.puttTrainerId = user.puttTrainers[0].puttTrainerId;
        APIGlobal.Inst.token = user.token;


        UIPopup.HidePopup("AlertPopup");
        UIView.HideView("RoboGreen", "Login");

        UserLoggedInComplete?.Invoke();
    }


    public void LoadUserFromLocalFile()
    {
        if(PlayerPrefs.GetString("LoggedIn") == "True")
        {
            user.memberDetail.emailAddress = PlayerPrefs.GetString("Username");
            user.memberId = int.Parse(PlayerPrefs.GetString("MemberId"));
            user.token = PlayerPrefs.GetString("Token");
            user.dateTimeLoggedIn = PlayerPrefs.GetString("DateTimeLoggedIn");

            UserPuttTrainers puttTrainers = new UserPuttTrainers();
            puttTrainers.puttTrainerId = int.Parse(PlayerPrefs.GetString("PuttTrainerId"));
            puttTrainers.puttTrainerTypeId = int.Parse(PlayerPrefs.GetString("PuttTrainerTypeId"));
            puttTrainers.puttTrainerTypeName = PlayerPrefs.GetString("PuttTrainerTypeName");
            puttTrainers.name = PlayerPrefs.GetString("PuttTrainerName");

            user.puttTrainers[0] = puttTrainers;
            
            APIGlobal.Inst.puttTrainerId = puttTrainers.puttTrainerId;
            APIGlobal.Inst.token = user.token;
            UserLoggedInComplete?.Invoke();

            if(CheckForRefreshData())
            {
                LogoutUserDataFromLocalFile();
            }
        }
        else
        {
            login.Show();
        }
    }

    public void LogoutUserDataFromLocalFile()
    {
        PlayerPrefs.SetString("LoggedIn", "False");
        PlayerPrefs.SetString("Username", "");
        PlayerPrefs.SetString("Password", "");
        PlayerPrefs.SetString("Token", "");
        PlayerPrefs.SetString("DateTimeLoggedIn", "");
        PlayerPrefs.SetString("PuttTrainerId", "");
        PlayerPrefs.SetString("PuttTrainerTypeId", "");
        PlayerPrefs.SetString("PuttTrainerTypeName", "");
        PlayerPrefs.SetString("PuttTrainerName", "");
        PlayerPrefs.SetString("MemberId", "");

        user = null;
        UIView.ShowView("RoboGreen", "Login");
        UIView.HideView("RoboGreen", "Dashboard");
    }

    public bool CheckForRefreshData()
    {
        bool forceLogin = false;
        DateTime todaysDate = DateTime.Now;
        DateTime lastLoggedInDate = DateTime.Parse(user.dateTimeLoggedIn);
        TimeSpan elapsedTime = todaysDate - lastLoggedInDate;

        if(elapsedTime.Days > 300)
        {
            forceLogin = true;
        }
        return forceLogin;
    }



}
