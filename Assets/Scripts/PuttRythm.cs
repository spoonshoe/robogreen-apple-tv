﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PuttRythm : MonoBehaviour
{

    public GameObject puttRythmGO;
    public Tween tween;
    public Transform target;
    public Vector3 targetPos;
    public Vector3 origin;


    public GameObject targeBehindBallCube;
    public GameObject targetHoleCube;

    public float speed;

    public Vector3 puttRythmLineVector;
    public GameObject stanceRightHandedLine;
    public GameObject stanceLeftHandedLine;

    public enum Direction
    {
        In, Out
    }
    public Direction direction = Direction.Out;



    void Update()
    {
        if(!target)
        {
            return;
        }

        //per putt?? instanitae with simulation

        float step = 2 * Time.deltaTime;
        if(direction == Direction.In)
        {
            targetPos = targetHoleCube.transform.position;
        }

        if(direction == Direction.Out)
        {
            targetPos = targeBehindBallCube.transform.position;
    
        }

        float distanceToTarget = Vector3.Distance(puttRythmGO.transform.position, targetPos);
        if(distanceToTarget < .1f)
        {
            if(direction == Direction.In)
            {
                direction = Direction.Out;
            }else
            {
                direction = Direction.In;
            }

        }
        puttRythmGO.transform.position = Vector3.MoveTowards(puttRythmGO.transform.position, targetPos, step);
    }

    void SetBoxForHole()
    {
         targetHoleCube.transform.localPosition = targetHoleCube.transform.localPosition - Vector3.forward * .5f;
         targeBehindBallCube.transform.localPosition = targeBehindBallCube.transform.localPosition + Vector3.forward * .5f;


        Vector3 currentVector = targetHoleCube.transform.position - targeBehindBallCube.transform.position;
        Vector3 extendedVector = currentVector * (2f);
        puttRythmLineVector = extendedVector;

        Debug.DrawRay(targetHoleCube.transform.position, extendedVector);

        stanceRightHandedLine.transform.localPosition = targeBehindBallCube.transform.localPosition - Vector3.left * 2;
        stanceLeftHandedLine.transform.localPosition = targeBehindBallCube.transform.localPosition - Vector3.right * 2;
    }


    public void ShowHideRythm(bool onOff)
    {
        puttRythmGO.SetActive(onOff);
    }
}
