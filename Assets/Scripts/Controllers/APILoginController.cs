﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Strackaline;
using Doozy.Engine;
using System.Text;

public class APILoginController : MonoBehaviour
{
    public string PostLoginURL;
    public UnityWebRequest apiRequest;
    public UnityWebRequest memberAPIRequest;
    public UserController userController;

    public void Awake()
    {
        userController = FindObjectOfType<UserController>();
        System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) =>
        {
            return true;
        };
    }


    public void AccountLogin(UserSubmitData _userSubmitData)
    {
        byte[] formData = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(_userSubmitData));

        string finalURL = APIGlobal.Inst.baseURL + PostLoginURL;

        apiRequest = UnityWebRequest.Put(finalURL, formData);
        apiRequest.method = UnityWebRequest.kHttpVerbPOST;
        apiRequest.SetRequestHeader("Content-Type", "application/json");
        apiRequest.SetRequestHeader("Accept", "application/json");
        StartCoroutine(LoginRequest(_userSubmitData));
    }

    IEnumerator LoginRequest(UserSubmitData _userSubmitData)
    {
        using (apiRequest)
        {
            yield return apiRequest.SendWebRequest();
            if (apiRequest.isNetworkError || apiRequest.isHttpError)
            {
                FindObjectOfType<AlertPopupController>().ShowAlertPopup("Login Failure", "Your username or password are incorrect.");
            }
            else
            {
                FindObjectOfType<LoginValidateCredentials>().User = JsonUtility.FromJson<User>(apiRequest.downloadHandler.text);
                userController.user = JsonUtility.FromJson<User>(apiRequest.downloadHandler.text);
                APIGlobal.Inst.token = userController.user.token;
                GetMemberDetail(userController.user.memberId);
            }
        }

    }

    public void GetMemberDetail(int _memberId)
    {
        string finalURL = APIGlobal.Inst.baseURL + "GetMember(memberId=" + _memberId + ")";
        memberAPIRequest = UnityWebRequest.Get(finalURL);
        memberAPIRequest.SetRequestHeader("Content-Type", "application/json");
        memberAPIRequest.SetRequestHeader("Authorization", "Token " + APIGlobal.Inst.apiToken);

        StartCoroutine(WaitForMemberDetailData());
    }


    IEnumerator WaitForMemberDetailData()
    {
        using (memberAPIRequest)
        {
            yield return memberAPIRequest.SendWebRequest();
            if (memberAPIRequest.isNetworkError || memberAPIRequest.isHttpError)
            {
                FindObjectOfType<AlertPopupController>().ShowAlertPopup("Error", "Unable to retrieve memeber data.");
            }
            else
            {
                User tempUser = new User();
                tempUser = JsonUtility.FromJson<User>(memberAPIRequest.downloadHandler.text);
                userController.user.puttTrainers = tempUser.puttTrainers;
                userController.SaveUserToLocalFile(JsonUtility.ToJson(userController.user));
                FindObjectOfType<AlertPopupController>().ShowAlertPopup("Login Success", "You have been logged in.");
            }
        }
    }


}
