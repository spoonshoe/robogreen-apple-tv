﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.UI;
using UnityEngine.UI;
using TMPro;

public class AlertPopupController : MonoBehaviour
{
    private static AlertPopupController inst;
    public static AlertPopupController Instance { get { return inst; } }


    public string popupName = "AlertPopup";
    public UIPopup alertPopup;

    private void Awake()
    {
        if (inst != null && inst != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            inst = this;
        }
    }

    public void ShowAlertPopup(string _header, string _body) 
    {
        alertPopup = UIPopupManager.ShowPopup(popupName, true, false);
        alertPopup.Data.Labels[0].GetComponent<TextMeshProUGUI>().text = _header;
        alertPopup.Data.Labels[1].GetComponent<TextMeshProUGUI>().text = _body;
        SetBtnEvents();
    }

    void SetBtnEvents()
    {
        alertPopup.Data.Buttons[0].GetComponent<Button>().onClick.AddListener(() => HidePopup(popupName));
        alertPopup.Data.Buttons[1].GetComponent<Button>().onClick.AddListener(() => HidePopup(popupName));
    }

    public void HidePopup(string popupName)
    {
        Debug.Log("hide popup");
        UIPopup.HidePopup(popupName);
    }
}
