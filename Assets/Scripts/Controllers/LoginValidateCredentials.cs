﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Strackaline;
using System;

public class LoginValidateCredentials : MonoBehaviour
{
    public static event Action UserDataSet;

    public TMP_InputField userNameIF;
    public TMP_InputField passwordIF;
    public Button submitBtn;


    private User user;
    [SerializeField]
    public User User
    {
        get => user;
        set
        {
            user = value;
            UserDataSet?.Invoke();
        }
    }

    private void Awake()
    {
        SetEvents();
    }

    public void SetEvents()
    {
        submitBtn.onClick.AddListener(ValidateLogin);
    }

    public void RemoveEvents()
    {
        submitBtn.onClick.RemoveAllListeners();
    }

  
    public void ValidateLogin()
    {
        UserSubmitData formData = new UserSubmitData();
        formData.userName = userNameIF.text;
        formData.password = passwordIF.text;
        FindObjectOfType<APILoginController>().AccountLogin(formData);
    }

    bool FieldValidation(TMP_InputField _InputField)
    {
        bool valid = false;
        if (_InputField.text.Length < 1 ||
            _InputField.text == "")
        {
            valid = false;
        }else
        {
            valid = true;
        }
        return valid;
    }
}
