﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowVis : MonoBehaviour
{

    public GameObject arrowContainer;
    public string eventName;


    private void Awake()
    {
        GreenViewOptionsController.SettingValueChanged += ArrowVisUpdate;
    }

    void ArrowVisUpdate(string _eventName, string _isOn)
    {
        if(_eventName == eventName)
        {
            if(_isOn == "On")
            {
                arrowContainer.SetActive(true);
            }
            else
            {
                arrowContainer.SetActive(false);
            }
        }
    }
}
