﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LineToTarget : MonoBehaviour
{
    private List<GameObject> pathToTargetMarkerList = new List<GameObject>();
    public Vector3 startLinePosition;
    public Vector3 endLinePosition;

    public GameObject targetLineSegmentPrefab;

    public GameObject targetPrefabInst;
    public GameObject targetPrefab;
    public PuttSimulation puttSimulation;

    private void Awake()
    {
        puttSimulation = GetComponent<PuttSimulation>();
    }

    private void OnDisable()
    {
        ClearLineToTarget();
    }

    void PlaceTarget()
    {
        targetPrefabInst = Instantiate(targetPrefab);
        targetPrefabInst.transform.position = new Vector3(puttSimulation.putt.target.x, puttSimulation.putt.target.z, puttSimulation.putt.target.y);
    }

    public void SetLineToTargetMarkers()
    {
        PlaceTarget();

        Debug.Log("SetLineToTargetMarkers");
        startLinePosition = new Vector3(puttSimulation.puttPointsFull[0].x, puttSimulation.puttPointsFull[0].y, puttSimulation.puttPointsFull[0].z);

        Vector3 p1 = startLinePosition;
        Vector3 p2 = targetPrefabInst.transform.position;


        float maxSpacing = .3f;
        float maxDistance = 1f;

        Vector3 diff = p2 - p1;
        Vector3 dir = diff.normalized;

        float totalDistance = diff.magnitude;

        for (float dist = 0; dist < totalDistance; dist += maxSpacing)
        {
            Vector3 v = p1 + dir * dist;
            GameObject go = Instantiate(targetLineSegmentPrefab, Vector3.zero, Quaternion.identity);

            go.transform.LookAt(targetPrefabInst.transform.position);
            go.transform.position = new Vector3(v.x, v.y, v.z);
            pathToTargetMarkerList.Add(go);
        }
    }

    private void Update()
    {
        if(pathToTargetMarkerList.Count > 0)
        {
            foreach (var item in pathToTargetMarkerList)
            {
                item.transform.LookAt(targetPrefabInst.transform);
            }
        }

    }

    public void ClearLineToTarget()
    {
        foreach (var item in pathToTargetMarkerList)
        {
            Destroy(item.gameObject);
        }
        pathToTargetMarkerList.Clear();
        Destroy(targetPrefabInst);
    }
}
