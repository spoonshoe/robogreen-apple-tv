﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;
public class ContourManager : MonoBehaviour
{

    public List<GameObject> contourLineList = new List<GameObject>();
    public GameObject contourCont;
    public Material lineMat;
    public List<ContourPath> contourPaths = new List<ContourPath>();

    private void Awake()
    {
        contourCont = GameObject.Find("ContourContainer");
        APIElevationDataController.APIElevationDataLoaded += LoadContourLines;
    }

    public void UnloadContourLines()
    {
        contourCont.transform.Clear();
    }


    public void LoadContourLines()
    {
        UnloadContourLines();
        foreach (ContourPath contourPath in contourPaths)
        {
            List<Vector3> contourLinePositions = new List<Vector3>();
            GameObject go = new GameObject();
            go.transform.SetParent(contourCont.transform);
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;

            LineRenderer lineRenderer = go.AddComponent<LineRenderer>();
            lineRenderer.material = lineMat;
            lineRenderer.receiveShadows = false;
            lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            lineRenderer.positionCount = contourPath.path.Length;
            lineRenderer.startWidth = 0.01f;
            lineRenderer.endWidth = 0.01f;
            lineRenderer.useWorldSpace = false;
            lineRenderer.generateLightingData = true;
            int i = 0;

            foreach (var seg in contourPath.path)
            {   
                contourLinePositions.Add(new Vector3(seg.x, seg.z, seg.y));
                lineRenderer.SetPositions(contourLinePositions.ToArray());
                i++;
            }
            contourLineList.Add(go);
        }
    }
}
