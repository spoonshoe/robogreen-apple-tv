﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationManager : MonoBehaviour
{
    public bool loopPutt;
    public enum PuttLineSpeed
    {
        Slow, Normal, Fast
    }
    public PuttLineSpeed puttLineSpeed;

    private void Awake()
    {
      //  SimulationViewController.LoopPuttEvent += LoopToggle;
    }

    void LoopToggle()
    {
        loopPutt = !loopPutt;
    }

}
