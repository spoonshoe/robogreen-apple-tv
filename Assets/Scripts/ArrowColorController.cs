﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;

public class ArrowColorController : MonoBehaviour
{
    public string eventName;
    public ArrowManager arrowManager;
    public Color white;
    
    public void Awake()
    {
        arrowManager = GetComponent<ArrowManager>();
        GreenViewOptionsController.SettingValueChanged += ArrowColorEvent;
    }

    void ArrowColorEvent(string _eventName, string _isOn)
    {
        if(_eventName == eventName)
        {
            if(_isOn == "On")
            {
                ArrowColorUpdate("multicolor");
            }
            else
            {
                ArrowColorUpdate("white");
            }
        }
    }

    void ArrowColorUpdate(string _color)
    {
		if (_color == "white")
		{
			foreach (var item in arrowManager.arrowList)
			{
					item.transform.GetChild(0).GetComponent<SpriteRenderer>().color = white;
            }
			return;
		}

		if (_color == "multicolor")
		{
			foreach (var item in arrowManager.arrowList)
			{
                Arrow arrow = item.transform.GetChild(0).GetComponent<ArrowPrefabController>().arrow;
                Color arrowColor = new Color(Mathf.Ceil(arrow.color.r), Mathf.Ceil(arrow.color.g), Mathf.Ceil(arrow.color.b), 1f);
				if (ColorUtility.TryParseHtmlString(arrow.color.hex, out arrowColor))
				{
					item.transform.GetChild(0).GetComponent<SpriteRenderer>().color = arrowColor;
				}
            }
			return;
		}
    }
}
