﻿using UnityEngine;
using UnityEngine.tvOS;
using Rewired;
using Doozy.Engine;
using Doozy.Engine.UI;
using Doozy.Integrations;

public class AppleTVRemoteController : MonoBehaviour
{
    public int playerId = 0;
    private Player player;
    public bool selected;
    public bool showDashboard;


    private void Awake()
    {
        player = ReInput.players.GetPlayer(playerId);
        UnityEngine.tvOS.Remote.allowExitToHome = true;
    }

    void Update()
    {
        if (player.GetButtonDown("ShowLogin"))
        {
            UIView.ShowView("RoboGreen", "Login");
        }

        if(player.GetButtonDown("LogOut"))
        {
            FindObjectOfType<UserController>().LogoutUserDataFromLocalFile();
        }

        selected = player.GetButtonDown("Select");
        if (selected)
        {
            if (UIView.IsViewVisible("RoboGreen", "Dashboard"))
            {
                FindObjectOfType<UserController>().LogoutUserDataFromLocalFile();
              
            }else
            {
                UIPopup.HidePopup("AlertPopup");
            }
        }

        if (player.GetButtonDown("ShowDashboard"))
        {
            if (UIView.IsViewVisible("RoboGreen", "Dashboard"))
            {
                UIView.HideView("RoboGreen", "Dashboard");
            }
            else
            {
                UIView.ShowView("RoboGreen", "Dashboard");
            }
        }

    }

}
