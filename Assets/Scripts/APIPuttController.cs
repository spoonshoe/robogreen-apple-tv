﻿using System;
using System.Collections;
using System.Collections.Generic;
using APIPuttForm;
using UnityEngine;
using UnityEngine.Networking;
using Strackaline;
public class APIPuttController : MonoBehaviour
{

    public GameObject preloader;

    public UnityWebRequest apiRequest;
    public Dictionary<string, string> postHeaderDict = new Dictionary<string, string>();
    public CourseManager courseManager;
    public GameObject puttInteractivePrefab;
    public GameObject puttContainer;

    public void Awake()
    {
        courseManager = FindObjectOfType<CourseManager>();
        puttContainer = GameObject.Find("PuttContainer");
        APIProjectorDataController.ProjectoDataNeedsReload += LoadNewPutt;
    }


    private void OnDisable()
    {
        APIProjectorDataController.ProjectoDataNeedsReload -= LoadNewPutt;
    }

    void LoadNewPutt()
    {
        LoadPuttWithFirmness(GetComponent<PuttInteractive>().ballLocation, GetComponent<PuttInteractive>().holeLocation);
    }


    public void LoadPuttWithFirmness(Vector3 _ballLocation, Vector3 _holeLocation)
    {
       
        SendPuttForm _form = new SendPuttForm();
        _form.greenId = int.Parse(courseManager.selectedGreenID);
        Debug.Log("green id" + courseManager.selectedGreenID);
        _form.rotation = 0;

        List<SendPuttFormData> _dataList = new List<SendPuttFormData>();
        SendPuttFormData _data = new SendPuttFormData();

        APIPuttForm.HoleLocation holeLocation = new APIPuttForm.HoleLocation();
        APIPuttForm.BallLocation ballLocation = new APIPuttForm.BallLocation();

        _data.holeLocation = holeLocation;
        _data.ballLocation = ballLocation;

        ballLocation.x = _ballLocation.x;
        //have to invert y and z
        ballLocation.y = _ballLocation.z;

        holeLocation.x = _holeLocation.x;
        //have to invert y and z
        holeLocation.y = _holeLocation.z;


        _data.stimp = 11;
        _data.isMetric = true;
        _data.firmness = 0.0f;
        _dataList.Add(_data);
        _form.data = _dataList.ToArray();
           
        var formData = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(_form));
        string _postData = Convert.ToBase64String(formData);

        apiRequest = UnityWebRequest.Put(APIGlobal.Inst.baseURL + "GetPutt()?token=" + APIGlobal.Inst.token, formData);
        apiRequest.method = UnityWebRequest.kHttpVerbPOST;
        apiRequest.SetRequestHeader("Content-Type", "application/json");
        apiRequest.SetRequestHeader("Accept", "application/json");

        StartCoroutine(WaitForGetPutt());
    }



    IEnumerator WaitForGetPutt()
    {
        using (apiRequest)
        {
            yield return apiRequest.SendWebRequest();
            if (apiRequest.isNetworkError || apiRequest.isHttpError)
            {
                FindObjectOfType<AlertPopupController>().ShowAlertPopup("Error", "Unable to retrieve putt data.");
            }
            else
            {
               
                PuttVariation puttVariation = JsonUtility.FromJson<PuttVariation>(apiRequest.downloadHandler.text);
                float xball = puttVariation.value[0].ball.x;
                float zball = puttVariation.value[0].ball.y;

                transform.position = new Vector3(xball, 0, zball);
                transform.SetParent(puttContainer.transform);
                GetComponent<PuttInteractive>().PuttVariation = puttVariation;
            }
        }
    }
}
