﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;

public class HoleContainerController : MonoBehaviour
{
    public ProjectorController projectorController;
    public GameObject holeContainer;
    public GameObject holeRect;

    private void Awake()
    {
        projectorController = FindObjectOfType<ProjectorController>();
        APIProjectorDataController.ProjectoDataLoaded += UpdateCameraSettings;
    }

    public void UpdateCameraSettings()
    {
        HoleContainerSettings holeContainerSettings = projectorController.greenActivity.holeContainerSettings;
        holeContainer.transform.position = new Vector3(holeContainerSettings.xPos, holeContainerSettings.yPos, holeContainerSettings.zPos);
        holeRect.transform.rotation = Quaternion.Euler(0, holeContainerSettings.holeRectYRotation, 0);
    }
}
