﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;

public class GreenViewOptionsController : MonoBehaviour
{
    public delegate void SettingEvent(string _settingName, string _settingValue);
    public static SettingEvent SettingValueChanged;

    public ProjectorController projectorController;
    bool settingsLoaded;

    private void Awake()
    {
        projectorController = FindObjectOfType<ProjectorController>();
        APIProjectorDataController.ProjectionDataUpdatedComplete += LoadSettings;
       
    }

    void LoadSettings()
    {
        foreach (GreenSetting item in projectorController.settings.greenSettings)
        {
           SettingValueChanged?.Invoke(item.settingName, item.settingValue);
        }
    }

}
