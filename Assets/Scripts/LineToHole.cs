﻿using UnityEngine;
using Strackaline;

using System;
using System.Collections.Generic;

public class LineToHole : MonoBehaviour
{
    public Material lineMaterial;
    public GameObject lineToHoleGO;

    public int indexOfAnimationPoint;
    public List<Vector3> animatedLineToHoleList;
    public List<Vector3> puttPointsFull;
    public bool animateLineToHole;
    public LineRenderer lineRenderer;
    public SimulationManager simulationManager;
    public ProjectorController projectorController;
    private void Awake()
    {
        projectorController = FindObjectOfType<ProjectorController>();
        puttPointsFull = GetComponent<PuttSimulation>().puttPointsFull;
        lineRenderer = GetComponent<LineRenderer>();
        simulationManager = FindObjectOfType<SimulationManager>();
        APIProjectorDataController.ProjectoDataLoaded += CheckAnimationState;
        APIProjectorDataController.ProjectoDataNeedsReload += CheckAnimationState;
    }



    public void FixedUpdate()
    {
        if(!animateLineToHole)
        {
            return;
        }
        AnimateLine();
    }

    void CheckAnimationState()
    {
        animateLineToHole = projectorController.projectorStatusData.greenActivity.isPlaying;
    }

    public void AnimateLine()
    {

        if (!projectorController.projectorStatusData.greenActivity.isLooping)
        {
            if (indexOfAnimationPoint >= puttPointsFull.Count)
            {
                return;
            }
        }


        if (indexOfAnimationPoint >= puttPointsFull.Count)
        {   
            animatedLineToHoleList.Clear();
            indexOfAnimationPoint = 0;
            lineRenderer.SetPositions(animatedLineToHoleList.ToArray());
        }

        Vector3 ballNewPosition = new Vector3(puttPointsFull[indexOfAnimationPoint].x, puttPointsFull[indexOfAnimationPoint].y, puttPointsFull[indexOfAnimationPoint].z);
        animatedLineToHoleList.Add(ballNewPosition);

        lineRenderer.positionCount = indexOfAnimationPoint;

        if(indexOfAnimationPoint > 0)
        {
            lineRenderer.SetPosition(indexOfAnimationPoint-1, ballNewPosition);
        }
        indexOfAnimationPoint++;
    }

    void SetLineToHoleRenderSettings()
    {
        lineRenderer.startWidth = .01f;
        lineRenderer.endWidth = .01f;
        lineRenderer.material = lineMaterial;
    }

    void StartAnimateLineToHole()
    {
        animateLineToHole = !animateLineToHole;
        SetLineToHoleRenderSettings();
        lineRenderer.enabled = true;
    }

    void StopAnimateLineToHole()
    {
        animateLineToHole = false;
        lineRenderer.enabled = false;
    }

    public void DrawLineToHole()
    {
        SetLineToHoleRenderSettings();
        lineRenderer.positionCount = GetComponent<PuttSimulation>().puttPointsFull.Count;
        lineRenderer.SetPositions(GetComponent<PuttSimulation>().puttPointsFull.ToArray());
    }
}

