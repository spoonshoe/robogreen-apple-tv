﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Strackaline;

public class PuttAssistance : MonoBehaviour
{
    public GameObject puttRhythmPrefab;
    public PuttInteractive puttInteractive;
    public GameObject puttStanceBoxInst;
    public GameObject puttRhythmIcon;

    public GameObject stanceRightHandedLine;
    public GameObject stanceLeftHandedLine;

    public Vector3 directionOfHoletLocation;

    public int puttRhythmSpeed;
    public int puttStanceDistance;

    public string puttSpeedEventName;
    public string puttDistanceEventName;

    private void Awake()
    {
        puttInteractive = GetComponent<PuttInteractive>();
        GreenViewOptionsController.SettingValueChanged += PuttAssistanceEvent;
     }


    void PuttAssistanceEvent(string _eventName, string _value)
    {
        if (_eventName == puttSpeedEventName)
        {
             puttRhythmSpeed = int.Parse(_value);
        }

        if (_eventName == puttDistanceEventName)
        {
            puttStanceDistance = int.Parse(_value);
            SetBoxWidth();
        }
    }



    public void CreateStanceBox()
    {
        puttStanceBoxInst = Instantiate(puttRhythmPrefab);
        puttStanceBoxInst.transform.position = new Vector3(puttInteractive.puttSimulationsList[0].putt.ball.x,
                                            puttInteractive.puttSimulationsList[0].putt.ball.z,
                                            puttInteractive.puttSimulationsList[0].putt.ball.y);

        stanceRightHandedLine = puttStanceBoxInst.transform.Find("StanceRightHandedLine").gameObject;
        stanceLeftHandedLine = puttStanceBoxInst.transform.Find("StanceLeftHandedLine").gameObject;
        puttRhythmIcon = puttStanceBoxInst.transform.Find("PuttRythmIcon").gameObject;

        puttStanceBoxInst.transform.SetParent(this.transform);


        RotateStanceBoxTowardHole();
        SetBoxWidth();
        AnimateRhythymForPutt();
    }

    void RotateStanceBoxTowardHole()
    {
        directionOfHoletLocation = new Vector3(puttInteractive.puttSimulationsList[0].putt.hole.x, puttInteractive.puttSimulationsList[0].putt.hole.z, puttInteractive.puttSimulationsList[0].putt.hole.y);
        puttStanceBoxInst.transform.LookAt(directionOfHoletLocation);
    }


    void SetBoxWidth()
    {
        if (stanceRightHandedLine != null)
        {
            stanceRightHandedLine.transform.localPosition = new Vector3(puttStanceDistance * -.1f, 6, .05f);
            stanceLeftHandedLine.transform.localPosition = new Vector3(puttStanceDistance * .1f, 6, .05f);
        }
    }



    void AnimateRhythymForPutt()
    {
        puttRhythmIcon.transform.DOLocalMoveZ(-.8f, puttRhythmSpeed * .5f).SetLoops(2, LoopType.Yoyo).OnComplete(AnimateRhythmForPuttRestart);
       // Debug.Log("AnimateRhythymForPutt");
    }
    void AnimateRhythmForPuttRestart()
    {
        puttRhythmIcon.transform.DOLocalMoveZ(-.8f, puttRhythmSpeed * .5f).SetLoops(2, LoopType.Yoyo).OnComplete(AnimateRhythymForPutt);
       // Debug.Log("AnimateRhythmForPuttRestart");
    }



    public void ClearPuttStance()
    {
        Destroy(puttStanceBoxInst);
    }


}
