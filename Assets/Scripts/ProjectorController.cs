﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;

public class ProjectorController : MonoBehaviour
{
    public ProjectorStatusData projectorStatusData;
    public GreenActivity greenActivity;
    public Settings settings;
    public int configurationId;
}
