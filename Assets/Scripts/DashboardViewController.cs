﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Doozy.Engine.UI;
using Doozy.Engine;

public class DashboardViewController : MonoBehaviour
{

    public bool btnSelected;
    public TextMeshProUGUI userNameTxt;
    public TextMeshProUGUI locationTxt;
    public bool showDashboard;

    public void ViewInit()
    {
        userNameTxt.text = PlayerPrefs.GetString("Username");
    }


    public void LoginBtnClick()
    {
        UIView.ShowView("RoboGreen", "Login");
    }


}
