﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;
using UnityEngine.Events;
using System;

public class ArrowManager : MonoBehaviour
{
    public static event Action ArrowObjectsCreated;

    public GameObject arrowPrefab;
    public List<GameObject> arrowList = new List<GameObject>();
    public Color arrowColor;
    public Color arrowPrefColor;
    public GameObject arrowContGO;
    public bool isArrowAnimating = true;

    public GameObject arrowCont;
    public float maxArrowSpeed = 0;

    public List<Arrow> arrowCollectedList = new List<Arrow>();

    public void Awake()
    {
        arrowCont = GameObject.Find("ArrowContainer");
    }

    public void CreateArrowsForGreenElevationData()
    {
        UnloadArrows();
        CreateArrow(arrowCollectedList.ToArray());
    }

    public void UnloadArrows()
    {
        arrowCont.transform.Clear();
        arrowList.Clear();
    }

    public void CreateArrow(Arrow[] arrows)
    {
        foreach (var arrow in arrows)
        {
            GameObject arrowInst = Instantiate(arrowPrefab, new Vector3(arrow.point.x, arrow.point.z, arrow.point.y), Quaternion.identity);
            arrowInst.transform.localScale = new Vector3(.08f, .08f, .08f);
            arrowInst.transform.SetParent(arrowCont.transform);
            arrowInst.transform.localPosition = new Vector3(arrow.point.x, arrow.point.z, arrow.point.y);

            arrowInst.transform.GetChild(0).GetComponent<ArrowPrefabController>().speed = arrow.slope;
            arrowInst.transform.GetChild(0).GetComponent<ArrowPrefabController>().slope = arrow.slope;

            arrowColor = new Color(Mathf.Ceil(arrow.color.r), Mathf.Ceil(arrow.color.g), Mathf.Ceil(arrow.color.b), 1f);
            if (ColorUtility.TryParseHtmlString(arrow.color.hex, out arrowColor))
            {
                arrowInst.transform.GetChild(0).GetComponent<SpriteRenderer>().color = arrowColor;
            }


            arrowInst.transform.GetChild(0).GetComponent<ArrowPrefabController>().slopeColor = arrowColor;
            arrowInst.transform.GetChild(0).GetComponent<ArrowPrefabController>().hex = arrow.color.hex;

            arrowInst.transform.GetChild(0).GetComponent<Animator>().speed = arrowInst.transform.GetChild(0).GetComponent<ArrowPrefabController>().speed / 5;
            arrowInst.transform.localRotation = Quaternion.Euler(0, arrow.rotation, 0);
            arrowList.Add(arrowInst);

            if (maxArrowSpeed < arrowInst.transform.GetChild(0).GetComponent<Animator>().speed)
            {
                maxArrowSpeed = arrowInst.transform.GetChild(0).GetComponent<Animator>().speed;
            }
        }
       ArrowObjectsCreated?.Invoke();
    }


}

