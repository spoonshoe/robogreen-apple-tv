﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace APIPuttForm
{
    [System.Serializable]
    public class SendPuttForm
    {
        public int greenId;
        public float rotation;
        public SendPuttFormData[] data;
    }

    [System.Serializable]
    public class SendPuttFormData
    {
        public int stimp;
        public float firmness;
        public bool isMetric;
        public HoleLocation holeLocation;
        public BallLocation ballLocation;
    }

    [System.Serializable]
    public class HoleLocation
    {
        public float x;
        public float y;
    }

    [System.Serializable]
    public class BallLocation
    {
        public float x;
        public float y;
    }
}


//{
//greenId:794,
//rotation:125.0,

//data:[{
    //stimp:11,
    //firmness:2,
    //isMetric:false,
    //holeLocation:{
        //x:4.1,
        //y:4.6
    //},
    //ballLocation:{
        //x:0.1,
        //y:0.1
    //}
// }]
//}
