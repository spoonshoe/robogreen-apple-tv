﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Strackaline
{
    [System.Serializable]
    public class Region
    {
        public RegionData[] value;
    }

    [System.Serializable]
    public class RegionData
    {

        public string name;
        public string regionCode;
        public string countryCode;
        public string courseCount;
    }

}