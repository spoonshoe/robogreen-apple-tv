﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Strackaline
{
    [System.Serializable]
    public class GreenActivity
    {
        public string greenId;
        
        public float holeXPos;
        public float holeYPos;
        
        public bool isPlaying;
        public bool isLooping;
        public int speed;
        
        public GreenCameraSettings greenCameraSettings;
        public HoleContainerSettings holeContainerSettings;
        public GreenPutt[] greenPutts;
    }


    [System.Serializable]
    public class GreenPutt
    {
        public float ballXPos;
        public float ballYPos;
    }

    [System.Serializable]
    public class GreenCameraSettings
    {
        public float xPos;
        public float yPos;
        public float zPos;
        public float size;
        public float yRot;
    }

    [System.Serializable]
    public class HoleContainerSettings
    {
        public float xPos;
        public float yPos;
        public float zPos;
        public float holeRectYRotation;
    }

    //{"greenId": "123"}
}
