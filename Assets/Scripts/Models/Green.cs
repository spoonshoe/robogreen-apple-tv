﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Strackaline
{
    [System.Serializable]
    public class CourseGreens
    {
        public int courseId;
        public string name;
        public GreenGroup[] groups;
    }


    [System.Serializable]
    public class GreenGroup
    {
        public int groupId;
        public string name;
        public int sortOrder;
        public float stimp;
        public Green[] greens;

        public enum GreenGroupType
        {
            Practice, Course
        }
        public GreenGroupType greenGroupType;
    }

    [System.Serializable]
    public class Green
    {
        public HoleLocation[] holeLocations;
        public string name;
        public float rotation;
        public int greenId;
        public int groupId;
        public int number;
        public float latitude;
        public float longitude;
        public int trueNorth;
        public string DXFFile;
    }



    [System.Serializable]
    public class HoleLocation
    {
        public int id;
        public int greenId;
        public float rotation;
        public float x;
        public float y;
    }
}


//{
    //"@odata.context": "http://slapp.strackaline.com/api/V1/$metadata#SLN.BLL.CourseService.CourseDetailDto",
    //"@odata.type": "#SLN.BLL.CourseService.CourseDetailDto",
    //"courseId": 811,
    //"sourceId": 515,
    //"name": "Augusta Pines",
    //"address": "18 Augusta Pines Dr",
    //"city": "Spring",
    //"region": "TX",
    //"postalCode": "77389",
    //"countryCode": "US",
    //"profileImage": "https://www.strackaline.com/assets/course/811/CEIQhCEIQhCEIQv2Q1.jpg",
    //"latitude": 30.1189,
    //"longitude": -95.5166,
    //"isActive": true,
    //"settings": {
    //    "isHoleLocationSoftware": false,
    //    "holoVersion": 0,
    //    "isLockedFromSale": false,
    //    "customLockMessage": null,
    //    "lastPinsheetTemplateId": 0,
    //    "isCourseDataLocked": false,
    //    "isGreenDataLocked": false,
    //    "isGpsDataLocked": false
    //},
    //"groups": [
        //{
            //"groupId": 1643,
            //"name": "Course",
            //"sortOrder": 0,
            //"stimp": 10,
            //"memberStimp": 10,
            //"maxHoloSlope": 6.5,
            //"greens": [
                //{
                //    "greenId": 17055,
                //    "groupId": 1643,
                //    "number": 1,
                //    "par": null,
                //    "yardage": null,
                //    "name": "Hole 1",
                //    "isActive": true,
                //    "approachAngle": 166.5,
                //    "stimp": 10,
                //    "heading": 172.5,
                //    "trueNorth": 339,
                //    "latitude": 30.12635162,
                //    "longitude": -95.53596809,
                //    "approachLatitude": 30.1272703,
                //    "approachLongitude": -95.53610756,
                //    "maxSlope": 6.5,
                //    "holeLocation": {
                //        "holeNumber": 1,
                //        "greenId": 17055,
                //        "groupId": 1643,
                //        "approachAngle": 166.5,
                //        "on": 13.716,
                //        "left": 10.241,
                //        "right": 6.583,
                //        "createdOn": "2018-12-17T11:35:49.83-05:00",
                //        "edge": 6.497,
                //        "center": -1.865,
                //        "pinNumber": 0,
                //        "x": 3.446,
                //        "y": -1.865,
                //        "slope": 2.561,
                //        "holeLocationId": 1091518,
                //        "memberHoleLocation": {
                //            "holeLocationId": 0,
                //            "approachAngle": 0,
                //            "on": 0,
                //            "left": 0,
                //            "right": 0,
                //            "createdOn": null,
                //            "edge": null,
                //            "center": null,
                //            "x": 0,
                //            "y": 0,
                //            "slope": null
                //        }
                //    },
                //    "memberData": {
                //        "approachAngle": 166.5,
                //        "approachLatitude": null,
                //        "approachLongitude": null,
                //        "heading": 172.5,
                //        "stimp": 10
                //    },
                //    "greenDataProperties": [
                //        {
                //            "greenId": 17055,
                //            "greenDataId": 30402,
                //            "greenDataTypeId": 2,
                //            "greenDataPropertiesId": 0,
                //            "width": null,
                //            "height": null,
                //            "elevation": null,
                //            "trueNorth": 339,
                //            "latitude": 30.12635162,
                //            "longitude": -95.53596809,
                //            "isLocked": null
                //        }
                //    ]
                //},
