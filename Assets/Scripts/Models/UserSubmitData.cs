﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserSubmitData
{
    public string userName;
    public string password;
}
