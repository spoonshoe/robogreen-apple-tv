﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Strackaline
{
    [System.Serializable]
    public class ProjectorData
    {
        public Configurator[] value;
    }

    [System.Serializable]
    public class Configurator
    {
        public int puttTrainerId;
        public int configurationId;
        public string createdOn;
        public string configurationData;

    }
}


//    {
//    "@odata.context": "http://slapp.strackaline.com/api/V2/$metadata#Collection(SLN.BLL.PuttTrainerService.PuttTrainerDataDto)",
//    "value": [
//        {
//            "puttTrainerId": 1,
//            "configurationId": 46,
//            "createdOn": "2019-06-27T12:43:47.193-04:00",
//            "configurationData": "{data to post}"
//        }
//    ]
//}
