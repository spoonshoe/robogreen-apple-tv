﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;


[System.Serializable]
public class ProjectorStatusData
{
    public JackData jackData;
    public GreenActivity greenActivity;
    public Settings settings;
}
