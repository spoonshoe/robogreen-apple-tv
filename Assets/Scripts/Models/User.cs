﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Strackaline
{
    [System.Serializable]
    public class User
    {
        public string name;
        public string roboGreenID;
        public int memberId;
        public string token;
        public MemberDetail memberDetail;
        public UserPuttTrainers[] puttTrainers;
        public string dateTimeLoggedIn;
    }

    [System.Serializable]
    public class MemberDetail
    {
        public string emailAddress;
        public Roles[] roles;
        public UserCourse[] courses;
        public UserPrinter[] printers;
    }
    [System.Serializable]
    public class Roles
    {
        public int roleId;
        public string name;
        public string domain;
    }
    [System.Serializable]
    public class UserCourse
    {
        public int courseId;
        public int sourceId;
        public string name;
    }
    [System.Serializable]
    public class UserPrinter
    {
        public int printerId;
        public string name;
    }

    [System.Serializable]
    public class UserPuttTrainers
    {
        public int puttTrainerId;
        public string name;
        public int puttTrainerTypeId;
        public string puttTrainerTypeName;
    }
}







//{
//    "@odata.context": "http://www.strackaline.com/api/V1/$metadata#SLN.BLL.MemberService.MemberLoginDto",
//    "memberId": 51017,
//    "token": "78EA85C6-68EF-4E04-8DBA-3EA6BB8F3789",
//    "message": null,
//    "memberDetail": {
//        "memberId": 51017,
//        "name": "Aventine Studios",
//        "emailAddress": "aventine@strackaline.com",
//        "password": null,
//        "createdOn": "2018-06-11T10:44:21.55-04:00",
//        "expiresOn": "2019-06-11T10:45:00.42-04:00",
//        "roles": [
//            {
//                "roleId": 1,
//                "name": "General Member",
//                "domain": null
//            },
//            {
//                "roleId": 5,
//                "name": "Paid Subscriber",
//                "domain": null
//            }
//        ],
//        "courses": [
//            {
//                "courseId": 1,
//                "sourceId": 1622,
//                "name": "The Bridges at Rancho Santa Fe",
//                "address": null,
//                "city": "Rancho Santa Fe",
//                "region": "CA",
//                "postalCode": "92091",
//                "countryCode": null,
//                "profileImage": "https://www.strackaline.com/assets/course/1/iphone.jpg",
//                "latitude": 33.0206,
//                "longitude": -117.2032,
//                "isActive": false,
//                "settings": null,
//                "expiresOn": "2019-06-11T10:45:46.71-04:00",
//                "isSubscriptionActive": true
//            }
//        ],
//        "printers": [
//            {
//                "printerId": 35986,
//                "name": "NEW! - 18 Hole Location Sheet",
//                "formatId": 10,
//                "formatName": "18 Holes",
//                "pageSizeId": 1,
//                "pageSizeName": "Full Page (8.5 x 11)",
//                "estRenderTime": 160
//            }
//        ],
//        "prints": []
//    }
//}