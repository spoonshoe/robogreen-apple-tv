﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;


[System.Serializable]
public class ElevationTransferData
{
    public List<Arrow> arrowCollectedList = new List<Arrow>();
    public List<ContourPath> contourCollectedList = new List<ContourPath>();
}
