﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace Strackaline
{

    [System.Serializable]
    public class Course
    {
        public CourseData[] value;
    }

    [System.Serializable]
    public class CourseData
    {
        public int courseId;
        public int sourceId;
        public string name;
        public string address;

        public string city;
        public string region;
        public string postalCode;
        public string countryCode;
        public string profileImage;
        public double latitude;
        public double longitude;
        public bool isActive;
        public CourseSettings settings;
    }

    [System.Serializable]
    public class CourseSettings
    {
        public bool isHoleLocationSoftware;
        public int holoVersion;
        public bool isLockedFromSale;
        public string customLockMessage;
        public int lastPinsheetTemplateId;
        public bool isCourseDataLocked;
        public bool isGreenDataLocked;
        public bool isGpsDataLocked;

    }
}


//{
    //"@odata.context": "http://slapp.strackaline.com/api/V1/$metadata#Collection(SLN.BLL.CourseService.CourseDto)",
    //"value": [
        //{
        //    "courseId": 1359,
        //    "sourceId": 31979,
        //    "name": "Blessings Golf Club",
        //    "address": null,
        //    "city": "Fayetteville",
        //    "region": "AR",
        //    "postalCode": "72704",
        //    "countryCode": null,
        //    "profileImage": "https://www.strackaline.com/assets/course/1359/c14tjtvkohg.png",
        //    "latitude": 36.1329,
        //    "longitude": -94.2033,
        //    "isActive": true,
        //    "settings": {
        //        "isHoleLocationSoftware": false,
        //        "holoVersion": 0,
        //        "isLockedFromSale": true,
        //        "customLockMessage": null,
        //        "lastPinsheetTemplateId": 0,
        //        "isCourseDataLocked": false,
        //        "isGreenDataLocked": false,
        //        "isGpsDataLocked": false
        //    }
        //},