﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Strackaline
{
    [System.Serializable]
    public class GreenArrow
    {
        public Arrow[] arrows1;
        public Arrow[] arrows2;
        public Arrow[] arrows3;
    }

    [System.Serializable]
    public class Arrow
    {

        public ArrowLocation point;
        public float slope;
        public float rotation;
        public int level;

        public ArrowColor color;
    }

    [System.Serializable]
    public class ArrowColor
    {
        public float r;
        public float g;
        public float b;
        public float a;
        public string hex;

    }

     [System.Serializable]
    public class ArrowLocation
    {
        public float x;
        public float y;
        public float z;
    }

}



//],"arrows":[
    //{
    //  "point":{"x":-12.084290037002564,"y":-6.9431610740051273,"z":-0.9332609999999999 },
    //  "slope":8.69084644317627,"rotation":103.89433288574219,"level":0,//
    //   "color":{ "r":163.0915355682373,"g":163.0915355682373,"b":163.0915355682373,"a":1.0,"hex":"#A3A3A3"}
    //},{


//"arrows2":[
    //{
    //  "point":{
    //    "x":-8.3947791957655511,"y":-9.90197479677833,"z":0.0
    //  },"slope":3.1608421802520752,"rotation":152.01304626464844,"level":1,"color":null
    //},{