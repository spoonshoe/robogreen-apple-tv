﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Strackaline
{
    [System.Serializable]
    public class JackData
    {
        public Jack[] jacks;
        public int jackId;
        public string location;
    }

    [System.Serializable]
    public class Jack
    {
        public int index;
        public float elevation;
    }
}