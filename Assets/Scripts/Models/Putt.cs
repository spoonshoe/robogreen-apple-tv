﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Strackaline
{

    [System.Serializable]
    public class PuttVariation
    {
        public Putt[] value;


        private int puttVariationID;
        public int PuttVariationID
        {
            get { 
                if(puttVariationID < 1)
                {
                    int randNum = UnityEngine.Random.Range(10000, 90000);
                    return randNum;
                }
                return puttVariationID;
            }
            set
            {
                puttVariationID = value;
            }
        }
    }


    [System.Serializable]
    public class Putt
    {
        private int puttID;
        public int PuttID
        {
            get 
            {
                if (puttID < 1)
                {
                    int randNum = UnityEngine.Random.Range(10000, 90000);
                    return randNum;
                }
                return puttID;
            }
            set
            {
               puttID = value;
            }
        }


        public bool isValidPutt;
        public string message;
        public Ball ball;
        public Hole hole;
        public Target target;
        public float ballToHoleDistance;
        public float puttingDistance;
        public float correctionAngle;
        public float elevationChange;
        public float speedFactor;
        public Point[] points;
        public Instruction instructions;

    }

    [System.Serializable]
    public class Ball
    {
        public float x;
        public float y;
        public float z;
    }

    [System.Serializable]
    public class Hole
    {
        public float x;
        public float y;
        public float z;
    }

    [System.Serializable]
    public class Target
    {
        public float x;
        public float y;
        public float z;
    }

    [System.Serializable]
    public class Point
    {
        public float speed;
        public Location location;
        public float slope;
    }


    [System.Serializable]
    public class Location
    {

        public float x;
        public float y;
        public float z;

    }

    [System.Serializable]
    public class Instruction
    {
        //public string default;
        public string aim;
        public string weight;
        public string toHole;
        public string elevation;

        public string GetWeight(string _weight)
        {
            string str1 = _weight;
            string str2 = "/n";
            return str1.Replace(str2, " ");
        }
    }



}


//{
  //"@odata.context":"http://slapp.strackaline.com/api/V1/$metadata#Collection(SLN.Core.Green.PuttPath)","value":[
    //{
      //"isValidPutt":true,"message":"51ft 1in TO HOLE\nAim 8ft 4in LONG,\n1ft 1in RIGHT","instructions":{
      //  "default":"51ft 1in TO HOLE\nAim 8ft 4in LONG,\n1ft 1in RIGHT","aim":"1ft 1in RIGHT","weight":"8ft 4in LONG,","toHole":"51ft 1in","elevation":"13in UP"
      //},"aim":{
      //  "x":-13.332751488798959,"y":-0.66113799892431957,"z":0.0
      //},"ball":{
      //  "x":2.0,"y":2.0,"z":-0.42262500000000003
      //},"hole":{
      //  "x":-13.27467,"y":-0.9987937,"z":-0.10367866666666666
      //},"target":{
        //"x":-15.849958728137228,"y":-1.0980221316035719,"z":-0.010745666666666666




//{
  //"@odata.context":"http://slapp.strackaline.com/api/V1/$metadata#Collection(SLN.Core.Green.PuttPath)","value":[
    //{
      //"isValidPutt":true,"message":"11ft 5in TO HOLE,  1in DOWN\nAim 0ft LONG,\n1 Cup RIGHT","aim":{
      //  "x":-0.03603812388643314,"y":0.22713407424098631,"z":0.0
      //},"ball":{
      //  "x":2.0,"y":3.0,"z":-0.515454
      //},"hole":{
      //  "x":0.1,"y":0.1,"z":-0.55665999999999993
      //},"target":{
      //  "x":0.15503634494321283,"y":0.48735704240863953,"z":-0.55253333333333332
      //},"ballToHoleDistance":3.4672320277760473,"puttingDistance":3.4719672203063965,"elevationChange":-0.046808862968294229,"speedFactor":0.5,"points":[
        //{
        //  "location":{
        //    "x":1.9959334182413448,"y":2.994463197908908,"z":-0.51558444188483843
        //  },"speed":2.8192464178843912,"slope":2.63594595167715
        //},{
        //  "location":{
        //    "x":1.967600720931941,"y":2.9558323646240994,"z":-0.51649474219047753
        //  },"speed":2.8006743642285721,"slope":2.6373974221258694
        //},{
        //  "location":{
        //    "x":1.9395011757412144,"y":2.9174235849121217,"z":-0.51740014867160122
        //  },"speed":2.78210627315644,"slope":2.6361480421647543
        //},{.7116219658718457,"z":-0.5634398054408245