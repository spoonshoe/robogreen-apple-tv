﻿using System.Collections;
using System.Collections.Generic;
using Strackaline;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.Events;

public class APIElevationDataController : MonoBehaviour
{
    public static event Action APIElevationDataLoaded;

    public IEnumerator PingAPIForElevationDataCO;
    public bool checkForNewData;

    public ElevationTransferData elevationTransferData;

    public void Awake()
    {
        PingAPIForElevationDataCO = PingAPIForElevationData();
        UserController.UserLoggedInComplete += StartPingAPIForElevationData;
    }

    public void StartPingAPIForElevationData()
    {
        StartCoroutine(PingAPIForElevationDataCO);
    }

    IEnumerator PingAPIForElevationData()
    {
        while (checkForNewData)
        {
            yield return new WaitForSeconds(1);
            ElevationDataSubmit();
        }
    }


    public void ElevationDataSubmit()
    {
        string finalURL = APIGlobal.Inst.baseURL + "GetPuttTrainerConfig(puttTrainerId=" + APIGlobal.Inst.puttTrainerId + ",configTypeId=2)";

        UnityWebRequest apiRequest = UnityWebRequest.Get(finalURL);
        apiRequest.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        apiRequest.SetRequestHeader("Authorization", "Token " + APIGlobal.Inst.apiToken);
        StartCoroutine(ElevationDataRequest(apiRequest));
    }

    IEnumerator ElevationDataRequest(UnityWebRequest _webRequest)
    {
        using (_webRequest)
        {

            Debug.Log("ElevationDataRequest");
            yield return _webRequest.SendWebRequest();

            Debug.Log("Yield ElevationDataRequest");
            if (_webRequest.isNetworkError || _webRequest.isHttpError)
            {
                FindObjectOfType<AlertPopupController>().ShowAlertPopup("Error", "Unable to retrieve green data.");
            }
            else
            {
                Debug.Log("ElevationDataRequest .downloadHandler.text" + _webRequest.downloadHandler.text);
                if(!string.IsNullOrEmpty(_webRequest.downloadHandler.text))
                {
                    ProjectorData projectorData = JsonUtility.FromJson<ProjectorData>(_webRequest.downloadHandler.text);
                    FindObjectOfType<ElevationDataController>().projectorData = JsonUtility.FromJson<ProjectorData>(_webRequest.downloadHandler.text);

                    if (projectorData.value[0].configurationId != FindObjectOfType<ElevationDataController>().projectorElevationDataConfigId)
                    {
                        elevationTransferData = JsonUtility.FromJson<ElevationTransferData>(FindObjectOfType<ElevationDataController>().projectorData.value[0].configurationData);
                        FindObjectOfType<ArrowManager>().arrowCollectedList = elevationTransferData.arrowCollectedList;
                        FindObjectOfType<ArrowManager>().CreateArrowsForGreenElevationData();
                        FindObjectOfType<ContourManager>().contourPaths = elevationTransferData.contourCollectedList;
                        FindObjectOfType<ContourManager>().LoadContourLines();
                        FindObjectOfType<ElevationDataController>().projectorElevationDataConfigId = projectorData.value[0].configurationId;
                    }

                }
            }
        }
    }
}
