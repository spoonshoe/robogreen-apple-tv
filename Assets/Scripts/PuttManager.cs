﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Strackaline;

public class PuttManager : MonoBehaviour
{
    public ProjectorController projectorController;
    public GameObject holePrefab;
    public GameObject holeInst;
    public GameObject puttInteractivePrefab;
    public List<GameObject> puttInteractiveList = new List<GameObject>();
    public int puttSpeed;

    private void Awake()
    {
        projectorController = FindObjectOfType<ProjectorController>();
        APIProjectorDataController.ProjectoDataNeedsReload += GetDataForPutts;
        APIProjectorDataController.ProjectionDataUpdatedComplete += SetPuttAppSpeed;
    }



    void GetDataForPutts()
    {
        Vector3 holePos = new Vector3(projectorController.greenActivity.holeXPos, 0, projectorController.greenActivity.holeYPos);
        if(holeInst == null)
        {
            holeInst = Instantiate(holePrefab);
        }

        holeInst.transform.position = holePos;
        foreach (var item in puttInteractiveList)
        {
            Destroy(item);
        }
        puttInteractiveList.Clear();

        foreach (var item in projectorController.greenActivity.greenPutts)
        {
            Vector3 ballPos = new Vector3(item.ballXPos, 0, item.ballYPos);
            GameObject puttInteractiveInst = Instantiate(puttInteractivePrefab);
            puttInteractiveInst.GetComponent<PuttInteractive>().ballLocation = ballPos;
            puttInteractiveInst.GetComponent<PuttInteractive>().holeLocation = holePos;
            puttInteractiveInst.GetComponent<PuttInteractive>().LoadNewPutt();
            puttInteractiveList.Add(puttInteractiveInst);
        }
    }

    void SetPuttAppSpeed()
    {
        puttSpeed = projectorController.projectorStatusData.greenActivity.speed;
        switch (puttSpeed)
        {
            case 0:
                Time.fixedDeltaTime = 0.03f;
                break;
            case 1:
                Time.fixedDeltaTime = 0.009f;
                break;

            case 2:
                Time.fixedDeltaTime = 0.006f;
                break;
            default:
                break;
        }
    }

}
