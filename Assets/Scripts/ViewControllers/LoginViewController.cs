﻿using UnityEngine;

public class LoginViewController : MonoBehaviour
{
    public LoginValidateCredentials loginValidateCredentials;
    public void ViewInit()
    {
        string loginStatus = PlayerPrefs.GetString("LoggedIn");
        string username = PlayerPrefs.GetString("Username");
        string password = PlayerPrefs.GetString("Password");

        loginValidateCredentials.userNameIF.text = username;
        loginValidateCredentials.passwordIF.text = password;
    }

}
